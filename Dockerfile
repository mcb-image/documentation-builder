from ubuntu:xenial

run apt-get update \
    && apt-get install -y \
    software-properties-common build-essential python-software-properties

run LC_ALL=C.UTF-8 add-apt-repository ppa:rmescandon/yq \
    && apt-get update \
    && apt-get install -y \
    python-pip \
    python3-pip \
    wget \
    librsvg2-bin \
    wkhtmltopdf \
    texlive \
    texlive-xetex \
    texlive-latex-extra \
    texlive-fonts-extra \
    texlive-lang-german \
    texlive-lang-italian \
    openjdk-8-jdk \
    graphviz \
    curl \
    rubber \
    yq \
    npm \
    git \
    && rm -rf /var/lib/apt/lists/*

env PLANTUML_VERSION="1.2018.6"
run wget https://sourceforge.net/projects/plantuml/files/plantuml.$PLANTUML_VERSION.jar/download
run mkdir -p /opt/plantuml/bin
run mv download /opt/plantuml/bin/plantuml.jar
add plantuml /opt/plantuml/bin/plantuml
run chmod +x /opt/plantuml/bin/plantuml
run ln -s /opt/plantuml/bin/plantuml /usr/local/bin/plantuml

env PANDOC_VERSION=2.14.0.1
env PANDOC_RELEASE=1
env PANDOC_DEB=pandoc-$PANDOC_VERSION-$PANDOC_RELEASE-amd64.deb

env WKHTML_VERSION=0.12.6
env WKHTML_BIN=$WKHTML_VERSION.tar.gz

WORKDIR /opt

run wget https://github.com/wkhtmltopdf/wkhtmltopdf/archive/refs/tags/$WKHTML_VERSION.tar.gz
run tar xfz $WKHTML_BIN
run ln -s /opt/wkhtmltox/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf
run ln -s /opt/wkhtmltox/bin/wkhtmltoimage /usr/local/bin/wkhtmltoimage

run wget https://github.com/jgm/pandoc/releases/download/$PANDOC_VERSION/$PANDOC_DEB
run dpkg -i $PANDOC_DEB

run mkdir -p /root/.pandoc/templates
WORKDIR /root/.pandoc/templates
run wget https://raw.githubusercontent.com/tajmone/pandoc-goodies/master/templates/html5/github/GitHub.html5
run wget https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/master/eisvogel.tex
run wget https://raw.githubusercontent.com/aaronwolen/pandoc-letter/master/template-letter.tex
run wget https://raw.githubusercontent.com/benedictdudel/pandoc-letter-din5008/master/letter.latex
run wget https://raw.githubusercontent.com/mrzool/invoice-boilerplate/master/template.tex -O invoice.tex

run mkdir /data

run pip3 install MarkdownPP

WORKDIR /data

run npm install markdown-to-slides -g
